package com.classpath.reactor.core;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Supplier;

public class PublisherDemo {
    public static void main(String[] args) {
        //Publishers
        Mono<Integer> element = Mono.just(22);
        element.subscribe(System.out::println);
        Flux<Integer> values = Flux.just(12,33,12,45,34, 67,33,132);
        values.subscribe(v -> System.out.println(v));

        /*
            Predicate
            Supplier
            Consumer
            Function
            BiFunction
            BiConsumer
         */
    }
}
